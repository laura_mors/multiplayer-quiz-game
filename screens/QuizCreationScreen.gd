extends Control

const MAX_QUESTIONS = 10


var total_questions
var form_data = {}
var quiz_data = []
var question_media_path
var user_quizzes_scene = "res://screens/UserQuizzesScreen.tscn"
var create_server_scene = "res://screens/ServerCreationScreen.tscn"

onready var quiz_data_form = $QuizDataGrid
onready var file_dialog = $FileDialog
onready var question_number = $QuestionNumberBox/QuestionNumber
onready var question_text = $QuizDataGrid/QuestionText
onready var answer_texts = [$QuizDataGrid/AnswerText1, $QuizDataGrid/AnswerText2, 
							$QuizDataGrid/AnswerText3, $QuizDataGrid/AnswerText4]
onready var correct_answer_options = $QuizDataGrid/CorrectAnswer
onready var btn_next = $NextButton
onready var btn_save = $SaveButton

func _ready() -> void:
	for answer_txt in answer_texts:
		answer_txt.connect("text_changed", self, "_on_AnswerText_text_changed", [answer_txt])
	
	add_option_button_items()


func add_option_button_items():
	correct_answer_options.add_item("Seleziona la risposta corretta")
	correct_answer_options.add_item("1")
	correct_answer_options.add_item("2")
	correct_answer_options.add_item("3")
	correct_answer_options.add_item("4")
	correct_answer_options.select(0)


func _on_QuestionNumber_text_entered(new_text: String) -> void:
	if new_text.is_valid_integer() and int(new_text) <= MAX_QUESTIONS:
		total_questions = int(new_text)
		quiz_data_form.visible = true
		if total_questions == 1:
			btn_save.visible = true
		else:
			btn_next.visible = true
	else:
		$InvalidInputDialog.dialog_text = "Inserisci un numero valido. Le domande non possono essere più di 10."
		$InvalidInputDialog.popup_centered()


func _on_QuestionText_text_changed(new_text: String) -> void:
	form_data["Question"] = new_text


func _on_AddFileButton_button_up() -> void:
	file_dialog.popup()


func _on_FileDialog_file_selected(path: String) -> void:
	form_data["File"] = path
	$QuizDataGrid/AddFileButton.text = "Modifica file"


func _on_AnswerText_text_changed(new_text: String, node) -> void:
	var number = node.name.substr(node.name.length()-1)
	var key = "Answer_" + number
	form_data[key] = new_text


func _on_CorrectAnswer_item_selected(index: int) -> void:
	form_data["Correct_answer"] = correct_answer_options.get_item_text(index)


func _on_CancelButton_button_up() -> void:
	quiz_data.clear()
	quiz_data_form.visible = false
	btn_next.visible = false
	get_tree().change_scene(user_quizzes_scene)


func is_valid_form_data():
	var keys = ["Question", "Answer_1", "Answer_2", "Answer_3", "Answer_4", "Correct_answer"]
	if form_data.size() >= keys.size() and form_data.has_all(keys):
			for key in keys:
				if form_data[key] == "":
					return false
			return true
	
	return false


func is_last_question():
	if quiz_data.size() == total_questions - 1:
		return true
	return false


func _on_NextButton_button_up() -> void:
	if is_valid_form_data():
		quiz_data.append(form_data.duplicate())
		question_text.text = ""
		$QuizDataGrid/AddFileButton.text = "Scegli file"
		for answer in answer_texts:
			answer.text = ""
		correct_answer_options.select(0)
		
		form_data.clear()
		
		if is_last_question():
			btn_next.visible = false
			btn_save.visible = true
			
	else:
		$InvalidInputDialog.dialog_text = "Compila tutte le caselle."
		$InvalidInputDialog.popup_centered()


func _on_SaveButton_button_up() -> void:
	if is_valid_form_data():
		quiz_data.append(form_data.duplicate())
		
		$SaveConfirmationDialog.popup_centered()
	else:
		$InvalidInputDialog.dialog_text = "Compila tutte le caselle."
		$InvalidInputDialog.popup_centered()


func save_quiz_data():
	var new_quiz = QuizData.create_quiz(quiz_data)
	
	get_tree().change_scene(user_quizzes_scene)


func save_quiz_data_to_db() -> void:
	var dict = {}
	#dict["Desc"] = quiz_desc
	var date_created = OS.get_datetime()
	dict["Date_created"] = str(date_created.get("year")) + "-" + str(date_created.get("month")) + "-" + str(date_created.get("hour")) + ":" + str(date_created.get("minute")) + ":" + str(date_created.get("second"))
	dict["Player_ID"] = 1
	var success = Database.commit_data_to_DB("Quiz", dict)
	if not success: 
		print("Couldn't commit Quiz data to database")
	
	#Database.db.verbose_mode = true
	#var table_name = "Quiz"
	#var column_names = ["ID", "Date_created", "Player_ID"]
	#var query_string : String = "SELECT " + column_names[0] + " as ID FROM " + table_name + " WHERE " + column_names[1] + " = ? AND " + column_names[2] + " = ?;"
	#var param_bindings : Array = [dict["Date_created"], dict["Player_ID"]]
	#var ris = Database.db.query_with_bindings(query_string, param_bindings)
	#Database.db.query("select Quiz.ID as id from Quiz where Quiz.Date_created = \"" + dict["Date_created"] + "\" AND Quiz.Player_ID = " + dict["Player_ID"] + ";")
	#var quiz_id = Database.db.query_result[0]["ID"]
	var quiz_id = Database.db.last_insert_rowid
	dict.clear()
	
	for i in range(0, quiz_data.size()):
		dict["Text"] = quiz_data[i]["Question"]
		if quiz_data[i].has("File"):
			dict["File"] = quiz_data[i]["File"]
		else:
			dict["File"] = ""
		success = Database.commit_data_to_DB("Question", dict)
		if not success: 
			print("Couldn't commit Question data to database")
		
		#table_name = "Question"
		#column_names = ["ID", "Text", "File"]
		#query_string = "SELECT " + column_names[0] + " as ID FROM " + table_name + " WHERE " + column_names[1] + " = ? AND " + column_names[2] + " = ?;"
		#param_bindings = [dict["Text"], dict["File"]]
		#Database.db.query_with_bindings(query_string, param_bindings)
		#Database.db.query("select Question.ID as id from Question where Question.Text = \"" + dict["Text"] + "\" and Question.File = \"" + dict["File"] + "\";")
		#var question_id = Database.db.query_result[0]["ID"]
		var question_id = Database.db.last_insert_rowid
		dict.clear()
		
		dict["Quiz_ID"] = quiz_id
		dict["Question_ID"] = question_id
		success = Database.commit_data_to_DB("Quiz_questions", dict)
		if not success: 
			print("Couldn't commit Quiz_questions data to database")
		dict.clear()
		
		var index = 1
		for j in range(0, answer_texts.size()):
			var answer = "Answer_" + str(index)
			dict["Text"] = quiz_data[i][answer]
			if quiz_data[i]["Correct_answer"] == str(index):
				dict["Is_correct"] = 1
			else:
				dict["Is_correct"] = 0
			dict["Question_ID"] = question_id
			success = Database.commit_data_to_DB("Answer", dict)
			if not success: 
				print("Couldn't commit Answer data to database")
			dict.clear()
			index += 1
	
	#if QuizData.multiplayer_mode == true:
		#QuizData.quiz_id = quiz_id
		#get_tree().change_scene(create_server_scene)
	#else:
		#get_tree().change_scene(quiz_choice_scene)
