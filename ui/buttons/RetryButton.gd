extends Button

export(String, FILE) var next_scene_path: = ""

func _get_configuration_warning() -> String:
	if next_scene_path == "":
		return "The property Next Scene can't be empty" 
	else:
		return ""

func _on_RetryButton_button_up() -> void:
	PlayerData.reset_score()
	QuizData.reset_data()
	
	get_tree().change_scene(next_scene_path)
