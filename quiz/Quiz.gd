extends Control

var questions = []
var answers = []
var answer_index
var end_screen_path = "res://screens/EndScreen.tscn"

onready var match_type = $Header/MatchType
onready var countdown = $Header/Countdown
onready var countdown_text = $Header/Countdown/TimeLeft
onready var question_count = $Header/QuestionCount
onready var question_text = $QuestionPanel/QuestionText
onready var question_image = $QuestionPanel/Image
onready var answer_btns = [$AnswerPanel/Row1/Answer1, $AnswerPanel/Row1/Answer2, 
							$AnswerPanel/Row2/Answer3, $AnswerPanel/Row2/Answer4]


func _ready() -> void:
	QuizData.connect("next_question", self, "update_quiz_interface")
	QuizData.connect("solo_quiz_ended", self, "_on_solo_quiz_ended")
	if QuizData.multiplayer_mode == true:
		match_type.text = "Partita multiplayer"
		countdown.connect("countdown_ended", self, "_on_countdown_ended")
		countdown_text.visible = true
		Network.connect("disconnected", self, "_on_disconnected")
	
	QuizData.sort_quiz_content()
	questions = QuizData.questions
	answers = QuizData.answers
	answer_index = 0
	update_quiz_interface()


func update_quiz_interface():
		question_count.text = "Domanda %s" % (QuizData.question_index + 1) + " / %s" % QuizData.get_quiz_size()
		question_text.text = questions[QuizData.question_index]["Text"]
		
		if questions[QuizData.question_index]["File"]:
			load_image_data(questions[QuizData.question_index]["File"])
		
		for answer in answer_btns:
			answer.text = answers[answer_index]["Text"]
			answer_index += 1


func load_image_data(file_data):
	var width = file_data["width"]
	var height = file_data["height"]
	var format = file_data["format"]
	var data = file_data["data"]
	var buffer_len = file_data["buffer_len"]
	print("image data size: " + str(data.length()))
	var pba = Marshalls.base64_to_raw(data)
	pba = pba.decompress(buffer_len, File.COMPRESSION_ZSTD)
	
	var img = Image.new()
	img.create_from_data(width, height, false, format, pba)
	var texture = ImageTexture.new()
	texture.create_from_image(img, 0)
	
	update_texture(texture)


func update_texture(texture):
	question_image.visible = true
	if texture.get_size() < question_image.rect_size:
		question_image.stretch_mode = question_image.STRETCH_KEEP_CENTERED
	else:
		question_image.stretch_mode = question_image.STRETCH_KEEP_ASPECT_CENTERED
	question_image.texture = texture


func update_question_media(file_path):
	var file_check = File.new()
	if file_check.file_exists(file_path):
		var is_image = is_image(file_path)
		question_image.visible = is_image
		#question_video.visible = not question_image.visible
		if question_image.visible:
			var img : StreamTexture = load(file_path)
			if img.get_size() < question_image.rect_size:
				question_image.stretch_mode = question_image.STRETCH_KEEP_CENTERED
			else:
				question_image.stretch_mode = question_image.STRETCH_KEEP_ASPECT_CENTERED
			question_image.texture = load(file_path)
		#else:
			#question_video.stream = load(file_path)

# temp
func is_image(file_path):
	var ext = file_path.get_extension()
	if ext in ["png", "jpg", "jpeg"]:
		return true
	elif ext in ["webm", "ogv"]:
		return false


func _on_countdown_ended():
	QuizData.question_index += 1
	countdown.restart_timer(10)
	


func _on_disconnected():
	get_tree().change_scene("res://screens/MainScreen.tscn")


func _on_solo_quiz_ended():
	#var table_name = "Match"
	#var match_data = {}
	#match_data["Score"] = PlayerData.player_info.score
	#match_data["Player_ID"] = "1"
	#match_data["Quiz_ID"] = QuizData.quiz_id
	#var success = Database.commit_data_to_DB(table_name, match_data)
	#if not success:
		#print("Couldn't commit data to database.")
	get_tree().change_scene(end_screen_path)
