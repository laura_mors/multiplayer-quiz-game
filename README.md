# Multiplayer Quiz Game
Questo progetto consiste nella realizzazione di un gioco a quiz tramite l'ambiente di sviluppo Godot, con l'obiettivo di testarne il potenziale e acquisire conoscenza delle sue funzionalità.
Le principali funzionalità dell'applicazione creata includono: una modalità di gioco singola e una multiplayer; un meccanismo di autenticazione; la possibilità per gli utenti di creare e caricare quiz creati da loro; e un sistema per raccogliere i punteggi degli utenti in una classifica.

### Strumenti e risorse utilizzate
L'applicazione utilizza principalmente le risorse e funzioni offerte da Godot e già presenti all'interno del framework. In particolare, è stata impiegata la sua API di rete di alto livello per l'implementazione della parte multiplayer del gioco. Per dotare il programma di un sistema di autenticazione è stato utilizzato [SilentWolf](https://silentwolf.com/), un servizio di backend per Godot.

Sfondi creati da [Joh](https://johgames.itch.io/).


### Istruzioni per l'installazione
Per usufruire del programma, è sufficiente scaricare i file [multiplayer_quiz_game.exe](multiplayer_quiz_game.exe) e [multiplayer_quiz_game.pck](multiplayer_quiz_game.pck), per poi avviare l'eseguibile.


Per avere ulteriori informazioni sull'implementazione e sul funzionamento dell'applicazione, è possibile consultare il documento "[tesi.pdf](tesi.pdf)".