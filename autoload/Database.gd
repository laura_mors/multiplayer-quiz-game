extends Node

const SQLite = preload("res://addons/godot-sqlite/bin/gdsqlite.gdns")

var db
var db_name = "res://data/database.db"

func _ready() -> void:
	db = SQLite.new()
	db.path = db_name


func commit_data_to_DB(table_name, dict):
	db.open_db()
	return db.insert_row(table_name, dict)


func row_exists(table_name, column, value):
	db.open_db()
	return db.query_with_bindings("select " + column + " from " + table_name + " where " + column + " = ?;", [value])
