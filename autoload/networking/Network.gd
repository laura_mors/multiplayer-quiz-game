extends Node


signal server_created
signal joined_server
signal failed_to_join_server
signal failed_to_create_server
signal failed_to_create_client
signal player_list_changed
signal waiting_player_list_changed
signal removed_player(p_info)
signal countdown_time_initialized(time)
signal game_start
signal disconnected

var server_info = {
	name = "Server",
	max_players = 0,
	used_port = 0
}

var players
var ready_players
var latency : float = 0
var client_time_before_match
var time_before_match : float
var end_of_game 


func _ready() -> void:
	get_tree().connect("network_peer_connected", self, "_on_player_connected")
	get_tree().connect("network_peer_disconnected", self, "_on_player_disconnected")
	get_tree().connect("connected_to_server", self, "_on_connected_to_server")
	get_tree().connect("connection_failed", self, "_on_connection_failed")
	get_tree().connect("server_disconnected", self, "_on_server_disconnected")
	
	PlayerData.connect("score_updated", self, "_on_player_score_updated")
	QuizData.connect("multi_quiz_ended", self, "_on_multi_quiz_ended")


func initialize_game_info():
	players = {}
	ready_players = {}
	end_of_game = false


func start_server():
	var net = NetworkedMultiplayerENet.new()
	
	if net.create_server(server_info.used_port, server_info.max_players) != OK:
		emit_signal("failed_to_create_server")
		print("Failed to create server")
		return
	
	initialize_game_info()
	
	get_tree().set_network_peer(net)
	emit_signal("server_created")
	print("Server created")
	register_player(PlayerData.player_info)


func join_server(ip, port):
	var net = NetworkedMultiplayerENet.new()
	
	if net.create_client(ip, port) != OK:
		emit_signal("failed_to_create_client")
		print("Failed to create client")
		return
	
	initialize_game_info()
	
	get_tree().set_network_peer(net)
	print("Client created")


func _on_player_connected(id):
	pass


func _on_player_disconnected(id):
	print("Player " + str(id) + " disconnected from the server")
	if get_tree().is_network_server():
		unregister_player(id)
		rpc("unregister_player", id)


func _on_connected_to_server():
	emit_signal("joined_server")
	PlayerData.player_info.id = get_tree().get_network_unique_id()
	rpc_id(1, "get_server_time", OS.get_system_time_msecs())
	rpc_id(1, "register_player", PlayerData.player_info)
	register_player(PlayerData.player_info)


func _on_connection_failed():
	emit_signal("failed_to_join_server")
	get_tree().set_network_peer(null)


func _on_server_disconnected():
	print("Server disconnected")
	get_tree().set_network_peer(null)
	emit_signal("disconnected")
	players.clear()
	PlayerData.player_info.id = 1


func send_quiz_data_to_client(id, data):
	rpc_id(id, "quiz_data_received", data)


remote func quiz_data_received(data):
	QuizData.quiz_content = data.duplicate()


remote func get_server_time(client_time):
	print("Getting server time")
	var player_id = get_tree().get_rpc_sender_id()
	rpc_id(player_id, "return_server_time", OS.get_system_time_msecs(), client_time)


remote func return_server_time(server_time, client_time):
	print("Returning server time")
	latency = (OS.get_system_time_msecs() - client_time) / 2
	print("latency = " + str(latency))
	print("physics process delta = " + str(get_physics_process_delta_time()))
	latency = (latency / 1000) + get_physics_process_delta_time() 
	print("latency converted: " + str(latency))
	#client_time_before_match = client_time + latency


remote func register_player(new_player_info):
	if get_tree().is_network_server():
		for id in players:
			rpc_id(new_player_info.id, "register_player", players[id])
			if id != 1:
				rpc_id(id, "register_player", new_player_info)
	
	print("Registering player ", new_player_info.name, " (", new_player_info.id, ") to internal player table")
	players[new_player_info.id] = new_player_info
	emit_signal("player_list_changed")
	if get_tree().is_network_server():
		pre_configure_game(new_player_info.id)


remote func unregister_player(id):
	print("Removing player ", players[id].name, " from internal table")
	var p_info = players[id]
	ready_players.erase(id)
	players.erase(id)
	emit_signal("player_list_changed")
	emit_signal("removed_player", p_info)


remote func update_countdown_to_game_start(time):
	var new_time = time - latency
	#if round(new_time) > (new_time):
		#new_time -= 1
	print("time - latency =  " + str(new_time))
	new_time = int(new_time)
	print("rounded time - latency =  " + str(new_time))
	emit_signal("countdown_time_initialized", new_time)


remote func pre_configure_game(player_id):
	if player_id != 1:
		send_quiz_data_to_client(player_id, QuizData.quiz_content)
		rpc_id(player_id, "update_countdown_to_game_start", time_before_match)
	
	assert(get_tree().is_network_server())
	
	ready_players[player_id] = players[player_id]
	
	if ready_players.size() == server_info.max_players:
		ready_to_play()


remote func ready_to_play():
	assert(get_tree().is_network_server())
	rpc("start_game")
	start_game()


remote func start_game():
	emit_signal("game_start")
	yield(get_tree().create_timer(1.5), "timeout")
	ready_players.clear()
	get_tree().change_scene("res://quiz/Quiz.tscn")


func _on_player_score_updated():
	if QuizData.multiplayer_mode == true:
		if get_tree().is_network_server():
			update_scores(PlayerData.player_info)
		else:
			rpc_id(1, "update_scores", PlayerData.player_info)


remote func update_scores(player_info):
	if get_tree().is_network_server():
		for id in players:
			if id != 1 and id != player_info.id:
				rpc_id(id, "update_scores", player_info)
	
	players[player_info.id].score = player_info.score


func _on_multi_quiz_ended():
	end_of_game = true
	var waiting_room = load("res://screens/WaitingRoom.tscn").instance()
	get_parent().add_child(waiting_room)
	get_tree().change_scene_to(waiting_room)
	
	if PlayerData.player_info.id != 1:
		rpc_id(1, "pre_configure_game_end", PlayerData.player_info.id)
	pre_configure_game_end(PlayerData.player_info.id)


remote func pre_configure_game_end(player_id):
	if get_tree().is_network_server():
		for id in ready_players:
			if id != 1 and id != PlayerData.player_info.id:
				rpc_id(id, "pre_configure_game_end", player_id)

	print("Registering player ", players[player_id].name, " (", players[player_id].id, ") to ready player table")
	ready_players[player_id] = players[player_id]
	emit_signal("waiting_player_list_changed")
	if get_tree().is_network_server() and ready_players.size() == players.size():
		done_preconfiguring_game_end()


remote func done_preconfiguring_game_end():
	assert(get_tree().is_network_server())
	rpc("game_end")
	game_end()


remote func game_end():
	get_tree().change_scene("res://screens/EndScreen.tscn")
	var waiting_room = get_node_or_null("/root/WaitingRoom")
	if not waiting_room == null:
		waiting_room.queue_free()
	if get_tree().is_network_server():
		var peers = get_tree().get_network_connected_peers()
		if not peers.empty():
			for id in peers:
				rpc_id(id, "game_end")
			yield(get_tree().create_timer(1.5), "timeout")
	get_tree().get_network_peer().close_connection()
	get_tree().set_network_peer(null)
