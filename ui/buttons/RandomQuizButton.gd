extends Button

var quiz_scene = "res://quiz/Quiz.tscn"
var create_server_scene = "res://screens/ServerCreationScreen.tscn"

func _on_RandomQuizButton_button_up() -> void:
	#Database.db.open_db()
	#var table_name = "Quiz"
	#var column_name = "ID"
	#var query_string : String = "SELECT " + column_name + " FROM " + table_name + " ORDER BY RANDOM() LIMIT ?;"
	#var param_bindings : Array = [1]
	#Database.db.query_with_bindings(query_string, param_bindings)
	#QuizData.quiz_id = Database.db.query_result[0][column_name]
	var random_quiz
	var rng = RandomNumberGenerator.new()
	rng.randomize()
	var quiz_found = false
	yield(SilentWolf.Players.get_player_data("admin"), "sw_player_data_received")
	yield(get_tree().create_timer(1.0), "timeout")
	var users = SilentWolf.Players.player_data.Users
	while not quiz_found:
		var random_user = users[rng.randi() % users.size()]
		yield(SilentWolf.Players.get_player_data(random_user), "sw_player_data_received")
		yield(get_tree().create_timer(1.0), "timeout")
		var user_quizzes = SilentWolf.Players.player_data.Quizzes
		if user_quizzes.size() > 0:
			random_quiz = user_quizzes[rng.randi() % user_quizzes.size()]
			quiz_found = true
	QuizData.quiz_content = (random_quiz.values()[0]).duplicate()
	if QuizData.multiplayer_mode == true:
		get_tree().change_scene(create_server_scene)
	else:
		get_tree().change_scene(quiz_scene)
