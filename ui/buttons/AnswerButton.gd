extends Button

var points = 5

func _on_AnswerButton_button_up() -> void:
	if is_correct_answer(self.text):
		PlayerData.set_score(points)
	
	QuizData.question_index += 1


func is_correct_answer(btn_text):
	for index in range(0, QuizData.answers.size()):
		if QuizData.answers[index]["Text"] == btn_text:
			return QuizData.answers[index]["Is_correct"]
	
	#Database.db.open_db()
	#var table_name = "Answer"
	#var column_names = ["Is_correct", "ID"]
	#var query_string : String = "SELECT " + column_names[0] + " FROM " + table_name + " WHERE " + column_names[1] + " = ?;"
	#var param_bindings : Array = [answer_id]
	#Database.db.query_with_bindings(query_string, param_bindings)
	#return Database.db.query_result[0]["Is_correct"]
	
