extends Control

var quiz_scene = "res://quiz/Quiz.tscn"
var create_server_scene = "res://screens/ServerCreationScreen.tscn"
var next_scene = "res://screens/QuizChoiceScreen.tscn"

var first_login = true

onready var welcome_user_text = $WelcomeUserText
onready var processing_label = $ProcessingLabel
onready var play_menu = $PlayMenu
onready var auth_menu = $AuthMenu
onready var logout_button = $LogOutButton

func _ready() -> void:
	
	if first_login and SilentWolf.Auth.logged_in_player == null:
		yield(SilentWolf.Auth.auto_login_player(), "sw_session_check_complete")
		first_login = false
		PlayerData.player_info.name = SilentWolf.Auth.logged_in_player
		#yield(SilentWolf.Players.get_player_data(PlayerData.player_info.name), "sw_player_data_received")
	
	if SilentWolf.Auth.logged_in_player != null:
		toggle_ui_items_visibility(true)
		welcome_user_text.text = "Ciao, " + SilentWolf.Auth.logged_in_player + "!"
	else:
		toggle_ui_items_visibility(false)
	

func toggle_ui_items_visibility(value):
	play_menu.visible = value
	welcome_user_text.visible = value
	logout_button.visible = value
	auth_menu.visible = not value


func pick_random_quiz() -> void:
	#Database.db.open_db()
	#var table_name = "Quiz"
	#var column_name = "ID"
	#var query_string : String = "SELECT " + column_name + " FROM " + table_name + " ORDER BY RANDOM() LIMIT ?;"
	#var param_bindings : Array = [1]
	#Database.db.query_with_bindings(query_string, param_bindings)
	#QuizData.quiz_id = Database.db.query_result[0][column_name]
	processing_label.visible = true
	
	var rng = RandomNumberGenerator.new()
	rng.randomize()
	
	var random_quiz
	var quiz_found = false
	yield(SilentWolf.Players.get_player_data("admin"), "sw_player_data_received")
	yield(get_tree().create_timer(1.0), "timeout")
	var users = SilentWolf.Players.player_data.Users
	
	while not quiz_found:
		var random_user = users[rng.randi() % users.size()]
		yield(SilentWolf.Players.get_player_data(random_user), "sw_player_data_received")
		yield(get_tree().create_timer(1.0), "timeout")
		var user_quizzes = SilentWolf.Players.player_data.Quizzes
		if user_quizzes.size() > 0:
			random_quiz = user_quizzes[rng.randi() % user_quizzes.size()]
			quiz_found = true
	
	QuizData.quiz_content = (random_quiz.values()[0]).duplicate()
	processing_label.visible = false
	
	if QuizData.multiplayer_mode == true:
		get_tree().change_scene(create_server_scene)
	else:
		get_tree().change_scene(quiz_scene)


func _on_SinglePlayerModeButton_button_up() -> void:
	QuizData.multiplayer_mode = false
	pick_random_quiz()


func _on_MultiplayerModeButton_button_up() -> void:
	QuizData.multiplayer_mode = true
	pick_random_quiz()


func _on_LogOutButton_button_up() -> void:
	SilentWolf.Auth.logout_player()
	toggle_ui_items_visibility(false)
