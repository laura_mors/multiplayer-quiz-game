extends Node

signal countdown_updated(time)
signal countdown_ended

export(int) var count

onready var timer = $Timer
onready var time_left = $TimeLeft


func restart_timer(value):
	count = value
	timer.start()


func _on_Timer_timeout() -> void:
	time_left.text = "Tempo rimasto: " + str(count)
	emit_signal("countdown_updated", count)
	count -= 1
	if count < 0:
		timer.stop()
		emit_signal("countdown_ended")
