extends Control

var minimum_players = 2

onready var countdown = $Countdown
onready var local_player = $PlayerList/LocalPlayer
onready var other_players = $PlayerList/OtherPlayers


func _ready() -> void:
	Network.connect("player_list_changed", self, "_on_player_list_changed")
	Network.connect("waiting_player_list_changed", self, "_on_waiting_player_list_changed")
	Network.connect("disconnected", self, "_on_disconnected")
	Network.connect("game_start", self, "update_main_label")
	
	if get_tree().is_network_server():
		countdown.connect("countdown_updated", self, "_on_countdown_updated")
		#countdown.connect("countdown_ended", self, "_on_countdown_ended")
		#Network.connect("removed_player", self, "_on_removed_player")
	else:
		Network.connect("countdown_time_initialized", self, "inizialize_countdown")
	countdown.connect("countdown_ended", self, "_on_countdown_ended")
	
	local_player.text = PlayerData.player_info.name
	
	if Network.end_of_game == false: #Network.ready_players.empty():
		update_interface(Network.players)
		$LeaveButton.visible = true
	else:
		update_interface(Network.ready_players)


func update_interface(player_list):
	for c in other_players.get_children():
		c.queue_free()
	
	for p in player_list:
		if p != PlayerData.player_info.id:
			var new_label = Label.new()
			new_label.text = player_list[p].name
			other_players.add_child(new_label)


func _on_player_list_changed():
	print("The player list changed")
	
	if get_tree().is_network_server() and Network.players.size() == minimum_players:
		$StartMatchButton.visible = true
	
	update_interface(Network.players)


func _on_waiting_player_list_changed():
	print("The list of players who finished the quiz changed")
	
	update_interface(Network.ready_players)


func update_main_label():
	countdown.timer.stop()
	$Title.text = "La partita sta per iniziare..."


func _on_disconnected():
	get_tree().change_scene("res://screens/MainScreen.tscn")


func leave_room():
	get_tree().get_network_peer().close_connection()
	get_tree().set_network_peer(null)
	get_tree().change_scene("res://screens/MainScreen.tscn")


func _on_countdown_updated(time):
	Network.time_before_match = time


func _on_countdown_ended():
	if Network.end_of_game == false:
		if Network.players.size() < minimum_players:
			leave_room()
		else:
			Network.ready_to_play()
	else:
		Network.game_end()


func inizialize_countdown(time):
	countdown.count = time


func _on_StartMatchButton_button_up() -> void:
	assert(get_tree().is_network_server())
	Network.ready_to_play()
