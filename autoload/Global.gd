extends Node

var api_key : String


func _ready() -> void:
	var f = File.new()
	f.open('res://api_key.env', File.READ)
	api_key = f.get_line()
	f.close()

	SilentWolf.configure({
			"api_key": self.api_key,
			"game_id": "multiplayer_quiz_game",
			"game_version": "1.0.0",
			"log_level": 1
	})

	SilentWolf.configure_auth({
			"redirect_to_scene": "res://screens/MainScreen.tscn",
			"login_scene": "res://auth/Login.tscn",
			"reset_password_scene": "res://auth/ResetPassword.tscn",
			"session_duration_seconds": 0,
			"saved_session_expiration_days": 30
	})
	
	SilentWolf.configure_scores({
			"open_scene_on_close": "res://screens/MainScreen.tscn"
	})
