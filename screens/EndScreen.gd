extends Control

onready var local_player_result = $Results/LocalPlayerResult
onready var other_results = $Results/OtherPlayersResults

func _ready() -> void:
	local_player_result.text = local_player_result.text % PlayerData.player_info.score
		#for p in Network.players:
			#if p != PlayerData.player_info.id:
				#var new_label = Label.new()
				#new_label.text = "| " + Network.players[p].name + ": "  + str(Network.players[p].score) + " | "
				#other_results.add_child(new_label)
	if QuizData.multiplayer_mode == true:
		other_results.visible = true
		var p_results = create_sorted_players_results_array()
		for index in range(0, p_results.size()):
			var rank = index + 1
			var new_label = Label.new()
			new_label.text = str(rank) + ". " + p_results[index][0] + " - " + str(p_results[index][1])
			if PlayerData.player_info.name == p_results[index][0]:
				new_label.add_color_override("font_color", Color("ac2708"))
			other_results.add_child(new_label)
			new_label.align = Label.ALIGN_CENTER


class ScoreSorter:
	static func sort_descending_by_second_element(a, b):
		if a[1] > b[1]:
			return true
		return false

func create_sorted_players_results_array():
	var results = []
	for p in Network.players:
		results.append([Network.players[p].name, Network.players[p].score])
	
	results.sort_custom(ScoreSorter, "sort_descending_by_second_element")
	return results
