extends Node

signal score_updated

var player_info = {
	name = "Player",
	id = 1,
	created_quizzes = 0,
	score = 0,
	score_position = 0,
}


func _ready() -> void:
	SilentWolf.Auth.connect("sw_registration_succeeded", self, "_on_registration_succeeded")
	SilentWolf.Auth.connect("sw_login_succeeded", self, "_initialize_player_info")
	QuizData.connect("solo_quiz_ended", self, "save_score")
	QuizData.connect("multi_quiz_ended", self, "save_score")


func update_player_data(player_name, dict_key, new_data):
	yield(SilentWolf.Players.get_player_data(player_name), "sw_player_data_received")
	yield(get_tree().create_timer(1.0), "timeout")
	var dict = SilentWolf.Players.player_data.duplicate()
	dict[dict_key].append(new_data)
	SilentWolf.Players.post_player_data(player_name, dict)


func _initialize_player_info():
	player_info.name = SilentWolf.Auth.logged_in_player
	yield(SilentWolf.Players.get_player_data(player_info.name), "sw_player_data_received")
	if not SilentWolf.Players.player_data.empty() and not player_info.name == "admin":
		player_info.created_quizzes = SilentWolf.Players.player_data.Quizzes.size()
		#print("Player data: " + str(SilentWolf.Players.player_data))
	else:
		SilentWolf.Players.post_player_data(player_info.name, {"Quizzes": []})


func _on_registration_succeeded():
	_initialize_player_info()
	
	update_player_data("admin", "Users", player_info.name)


func delete_player_data(player_name):
	SilentWolf.Players.delete_all_player_data(player_name)


func set_score(new_value):
	player_info.score += new_value
	emit_signal("score_updated")


func save_score():
	yield(SilentWolf.Scores.get_score_position(player_info.score), "sw_position_received")
	player_info.score_position = SilentWolf.Scores.position
	var score_id = yield(SilentWolf.Scores.persist_score(player_info.name, player_info.score), "sw_score_posted")
	#print("Score persisted successfully: " + str(score_id))


func reset_score():
	self.player_info.score = 0
