extends "res://addons/silent_wolf/Auth/Login.gd"

var remember_me

func _on_RememberMeCheckBox_toggled(button_pressed: bool) -> void:
	remember_me = button_pressed
