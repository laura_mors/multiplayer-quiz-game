extends Control


func _ready() -> void:
	Network.connect("server_created", self, "_start_match")
	Network.connect("joined_server", self, "_start_match")
	Network.connect("failed_to_create_server", self, "_on_failed_to_create_server")
	Network.connect("failed_to_create_client", self, "_on_failed_to_create_client")
	Network.connect("failed_to_join_server", self, "_on_failed_to_join_server")
	
	$PlayerInfo/PlayerName.text = PlayerData.player_info.name
	if get_node_or_null("JoinServer/IP") != null:
		$JoinServer/IP.text = IP.get_local_addresses()[9] # get the local IP address


func _start_match():
	get_tree().change_scene("res://screens/WaitingRoom.tscn")


func _on_failed_to_create_server():
	$FailedToCreateServerDialog.popup_centered()
	

func _on_failed_to_create_client():
	$ClientErrorDialog.dialog_text = "Non è stato possibile creare un'istanza di tipo client."
	$ClientErrorDialog.popup_centered()


func _on_failed_to_join_server():
	$ClientErrorDialog.dialog_text = "Non è stato possibile unirsi al server. Controlla di aver inserito l'indirizzo IP e il numero di porta giusti."
	$ClientErrorDialog.popup_centered()


func _on_CreateServerButton_button_up() -> void:
	var server_name = $CreateServer/ServerName.text
	var server_port = $CreateServer/Port.text
	var max_players = $CreateServer/MaxPlayers.value
	
	if not server_name.empty() and server_port.is_valid_integer():
		Network.server_info.name = server_name
		Network.server_info.used_port = int(server_port)
		Network.server_info.max_players = int(max_players)
			
		QuizData.multiplayer_mode = true
	
		Network.start_server()
	else:
		$InvalidInputDialog.popup_centered()


func _on_JoinServerButton_button_up() -> void:
	var ip = $JoinServer/IP.text
	var join_server_port = $JoinServer/Port.text
	
	if ip.is_valid_ip_address() and join_server_port.is_valid_integer():
		QuizData.multiplayer_mode = true
	
		Network.join_server(ip, int(join_server_port))
	else:
		$InvalidInputDialog.popup_centered()
