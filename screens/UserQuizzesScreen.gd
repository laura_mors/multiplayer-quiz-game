extends Control

var user_quizzes
var quiz_array_index = 0
var quiz_id = 1


func delete_user_quizzes():
	PlayerData.delete_player_data(PlayerData.player_info.name)


func _on_DeleteQuizButton_button_up() -> void:
	$ConfirmationDialog.popup_centered()


func add_quiz_item_label(text):
	var new_label = Label.new()
	new_label.text = text #SilentWolf.Players.player_data.Quizzes[0]["1"]["Question_1"]
	$ScrollContainer/QuizContent.add_child(new_label)


func remove_quiz_item_labels():
	var labels = $ScrollContainer/QuizContent.get_children()
	for node in labels:
		node.queue_free()


func show_quiz(user_quiz):
	if not QuizData.quiz_content.empty():
		QuizData.reset_data()
	QuizData.quiz_content = user_quiz.duplicate()
	QuizData.sort_quiz_content()
	var questions = QuizData.questions
	var answers = QuizData.answers
	
	add_quiz_item_label("Quiz n. " + str(quiz_id))
	var a_index = 0
	var correct_answer
	for q_index in range(0, questions.size()):
		add_quiz_item_label("Domanda " + str(q_index + 1) + ": " + questions[q_index]["Text"])
		if questions[q_index]["File"]:
			add_quiz_item_label("(La domanda ha un file allegato.)")
			
		for answer_count in range(0, 4):
			add_quiz_item_label("Risposta " + str(answer_count + 1) + ": " + answers[a_index]["Text"])
			if answers[a_index]["Is_correct"]:
				correct_answer = answer_count
			a_index += 1
		
		add_quiz_item_label("Risposta corretta: " + str(correct_answer))


func _on_ShowQuizButton_button_up() -> void:
	yield(SilentWolf.Players.get_player_data(PlayerData.player_info.name), "sw_player_data_received")
	#yield(get_tree().create_timer(1.0), "timeout")
	$NextQuizButton.visible = true
	$PreviousQuizButton.visible = true
	if  not SilentWolf.Players.player_data.empty() and SilentWolf.Players.player_data.Quizzes.size() > 0:
		user_quizzes = SilentWolf.Players.player_data.Quizzes.duplicate()
		show_quiz(user_quizzes[quiz_array_index][str(quiz_id)])


func _on_NextQuizButton_button_up() -> void:
	if quiz_id < user_quizzes.size():
		remove_quiz_item_labels()
		QuizData.reset_data()
		quiz_array_index += 1
		quiz_id += 1
		show_quiz(user_quizzes[quiz_array_index][str(quiz_id)])


func _on_PreviousQuizButton_button_up() -> void:
	if quiz_id > 1:
		remove_quiz_item_labels()
		QuizData.reset_data()
		quiz_array_index -= 1
		quiz_id -= 1
		show_quiz(user_quizzes[quiz_array_index][str(quiz_id)])


func _on_GoBackButton_button_up() -> void:
	if not QuizData.quiz_content.empty():
		QuizData.reset_data()
	get_tree().change_scene("res://screens/MainScreen.tscn")
