extends Node

signal next_question
signal solo_quiz_ended
signal multi_quiz_ended

var multiplayer_mode : bool
var question_index = 0 setget set_question_index
var quiz_content = []
var questions = []
var answers = []


func _ready() -> void:
	self.question_index = 0


func create_question_dict_key(new_value):
	return "Question_" + str(new_value)


func create_question_file_dict_key(question_key):
	return question_key + "_file"


func create_answer_dict_key(new_value, question_index):
	return "Answer_" + str(question_index) + "_" + str(new_value)


func create_correct_answer_dict_key(question_index):
	return "Correct_answer_question_" + str(question_index)


func set_question_index(new_value):
	question_index = new_value
	if question_index > 0:
		if question_index < get_quiz_size():
			emit_signal("next_question")
		elif question_index == get_quiz_size():
			if multiplayer_mode == false:
				emit_signal("solo_quiz_ended")
			else:
				emit_signal("multi_quiz_ended")


func get_quiz_size():
	#Database.db.open_db()
	#var table_name = "Quiz_Questions"
	#var column_names = ["Question_ID", "Quiz_ID"]
	#var query_string : String = "SELECT COUNT(" + column_names[0] + ") AS questions FROM " + table_name + " WHERE " + column_names[1] + " = ?;"
	#var param_bindings : Array = [quiz_id]
	#Database.db.query_with_bindings(query_string, param_bindings)
	#Database.db.query("select count(question_id) as questions from " + table_name + " where quiz_id = " + str(quiz_id) + ";")
	#return Database.db.query_result[0]["questions"]
	return questions.size()


func save_image_data(file_path):
	var image = Image.new()
	var err = image.load(file_path)
	if err != OK:
		print("Couldn't load image")
		return
	
	var texture = ImageTexture.new()
	texture.create_from_image(image, 0)
	var img = texture.get_data()
	var img_data = {"width": img.get_width(), 
					"height": img.get_height(), 
					"format": img.get_format()}
	var img_bytes = img.get_data()
	print("image size: " + str(img_bytes.size()))
	img_data["buffer_len"] = img_bytes.size()
	img_bytes = img_bytes.compress(File.COMPRESSION_ZSTD)
	print("image size after compression: " + str(img_bytes.size()))
	img_data["data"] = Marshalls.raw_to_base64(img_bytes)
	
	return img_data


func copy_file_to_res(file_path):
	var dir = Directory.new()
	var new_path = "res://data/" + file_path.get_file()
	var err = dir.copy(file_path, new_path)
	if err != OK:
		return ""
	else:
		return new_path


func create_quiz(quiz_data):
	var new_quiz = {}
	
	var q_index
	for i in range(0, quiz_data.size()):
		q_index = i+1
		var question = create_question_dict_key(q_index)
		new_quiz[question] = quiz_data[i]["Question"]
		var question_file = create_question_file_dict_key(question)
		if quiz_data[i].has("File"):
			new_quiz[question_file] = save_image_data(quiz_data[i]["File"])
		else:
			new_quiz[question_file] = ""
		
		var a_index
		for j in range(0, 4):
			a_index = j+1
			var answer = create_answer_dict_key(a_index, q_index)
			new_quiz[answer] = quiz_data[i]["Answer_" + str(a_index)]
			a_index += 1
		var correct_answer = create_correct_answer_dict_key(q_index)
		new_quiz[correct_answer] = quiz_data[i]["Correct_answer"]
		#print("new quiz " + str(new_quiz))
	
	PlayerData.player_info.created_quizzes += 1
	var new_quiz_id = PlayerData.player_info.created_quizzes
	PlayerData.update_player_data(PlayerData.player_info.name, "Quizzes", {new_quiz_id: new_quiz})
	
	return new_quiz


func sort_quiz_content():
	var all_questions_found = false
	var q_index = 1
	var answer_index = 0
	while not all_questions_found:
		var question = create_question_dict_key(q_index)
		var question_file = create_question_file_dict_key(question)
		if question in quiz_content.keys():
			questions.append({"ID": q_index, 
								"Text": quiz_content[question],
								"File": quiz_content[question_file]})
			
			for a_index in range(1,5):
				var answer = create_answer_dict_key(a_index, q_index)
				if answer in quiz_content.keys():
					answers.append({"ID": a_index, 
									"Text": quiz_content[answer],
									"Is_correct": 0})
					var correct_answer = create_correct_answer_dict_key(q_index)
					if quiz_content[correct_answer] == str(a_index):
						answers[answer_index]["Is_correct"] = 1
				answer_index += 1
			q_index += 1
		else:
			all_questions_found = true



#func load_questions_from_DB():
	#Database.db.open_db()
	#var table_names = ["Question", "Quiz_Questions"]
	#var column_names = ["Question.ID", "Question.Text", "Question.File", "Quiz_Questions.Question_ID", "Quiz_Questions.Quiz_ID"]
	#var query_string : String = "SELECT " + column_names[0] + ", " + column_names[1] + ", " + column_names[2] + " FROM " + table_names[0] + " INNER JOIN " + table_names[1] + " ON " + column_names[0] + " = " + column_names[3] + " WHERE " + column_names[4] + " = ?;"
	#var param_bindings : Array = [quiz_id]
	#Database.db.query_with_bindings(query_string, param_bindings)
	#Database.db.query("select Question.ID, Question.Text, Question.File from Question INNER JOIN Quiz_Questions ON Question.ID = Quiz_Questions.Question_ID where Quiz_Questions.Quiz_ID = " + str(quiz_id) + ";")
	
	#for i in range(0, Database.db.query_result.size()):
		#questions.append({ "ID": Database.db.query_result[i]["ID"], "Text": Database.db.query_result[i]["Text"],
							#"File": Database.db.query_result[i]["File"]})
	#return questions


#func load_answers_from_DB(question_id):
	#if not answers.empty():
		#answers.clear()
		
	#Database.db.open_db()
	#var table_names = ["Answer", "Question"]
	#var column_names = ["Answer.ID", "Answer.Text", "Answer.Question_ID", "Question.ID"]
	#var query_string : String = "SELECT " + column_names[0] + ", " + column_names[1] + " FROM " + table_names[0] + " INNER JOIN " + table_names[1] + " ON " + column_names[3] + " = " + column_names[2] + " WHERE " + column_names[2] + " = ?;"
	#var param_bindings : Array = [question_id]
	#Database.db.query_with_bindings(query_string, param_bindings)
	#Database.db.query("select Answer.ID, Answer.Text from Answer INNER JOIN Question ON Question.ID = Answer.Question_ID where Answer.Question_ID = " + str(question_id) + ";")
	
	#for i in range(0, Database.db.query_result.size()):
		#answers.append({ "ID": Database.db.query_result[i]["ID"], "Text": Database.db.query_result[i]["Text"]})
	#return answers


func reset_data():
	self.question_index = 0
	questions.clear()
	answers.clear()
