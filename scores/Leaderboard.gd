extends "res://addons/silent_wolf/Scores/Leaderboard.gd"

var leaderboard_max_rows = 10


func _ready() -> void:
	if PlayerData.player_info.score_position > leaderboard_max_rows:
		#$Board/MessageContainer.visible = true
		#$Board/MessageContainer/TextMessage.text = "La tua posizione: " + str(PlayerData.player_info.score_position)
		$Board/TitleContainer/Label.text = $Board/TitleContainer/Label.text + "(La tua posizione: " + str(PlayerData.player_info.score_position) + ")\n"
